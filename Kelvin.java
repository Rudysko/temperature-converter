public class Kelvin{
  public double KelvToCel(double Kelv){
    double KelvToCel = Kelv - 273.15;
    return KelvToCel;

  }
  public double KelvToFahr(double Kelv){
    double KelvToFahr = (Kelv * 1.8) - 459.67;
    return KelvToFahr;

  }
}
